       IDENTIFICATION DIVISION. 
       PROGRAM-ID. EDIT5.
       AUTHOR. NARANYA  BOONMEE.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SmallerScaledNumber  PIC   VP(5)999 VALUE .00000423.
       01  LargeScaledNumber    PIC   999P(5)V VALUE 45600000.00.
       01  ScaledBillions       PIC   999P(5) VALUE ZERO .

       01  SmallNumber          PIC   9V9(8)   VALUE 1.1111111.
       01  LargeNumber          PIC   9(8)V9   VALUE ZEROS.
       
       01  PrnSmall             PIC   99.9(8).
       01  PrnLarge             PIC   ZZ,ZZZ,ZZZ,ZZ9.
       01  PrnBillions          PIC   ZZZ,ZZZ,ZZZ,ZZ9.

       PROCEDURE DIVISION .
       Begin.
           MOVE SmallerScaledNumber TO PrnSmall
           MOVE LargeScaledNumber TO PrnLarge
           DISPLAY "Small scaled = " PrnSmall 
           DISPLAY "Large scaled = "  PrnLarge 

           ADD SmallerScaledNumber TO SmallNumber
           ADD LargeScaledNumber TO LargeNumber
           MOVE SmallNumber TO PrnSmall
           MOVE LargeNumber TO PrnLarge 
           DISPLAY "Small = " PrnSmall 
           DISPLAY "Large = " PrnLarge 

           MOVE 123456789012 TO ScaledBillions
           MOVE ScaledBillions TO PrnBillions
           DISPLAY "Billions = " PrnBillions 
           STOP RUN

           .
