       IDENTIFICATION DIVISION. 
       PROGRAM-ID. EDIT4.
       AUTHOR. NARANYA  BOONMEE.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Stars PIC   *****.
       01  NumOfStar   PIC   9.

       PROCEDURE DIVISION .
       Begin.
           PERFORM VARYING NumOfStar FROM 0 BY 1 UNTIL NumOfStar > 5
              COMPUTE Stars = 10 ** (4 - NumOfStar)
              INSPECT Stars CONVERTING "10" TO SPACES
              DISPLAY NumOfStar " = " Stars
           END-PERFORM
           STOP RUN

           .

           